//
//  FoolIsSearching.swift
//  
//
//  Created by foolbear on 2022/11/4.
//

import SwiftUI

@available(iOS 15.0, OSX 11.0, *)
struct FoolIsSearching: ViewModifier {
    @Binding var isSearching: Bool
    
    func body(content: Content) -> some View {
        content
            .background(FoolIsSearchingView(isSearching: $isSearching))
    }
    
    struct FoolIsSearchingView: View {
        @Environment(\.isSearching) private var isSearchingEnv
        @Binding var isSearching: Bool
        
        var body: some View {
            Color.clear.onChange(of: isSearchingEnv) { isSearching = $0 }
        }
    }
}

@available(iOS 15.0, OSX 11.0, *)
public extension View {
    func foolIsSearching(isSearching: Binding<Bool>) -> some View {
        self.modifier(FoolIsSearching(isSearching: isSearching))
    }
}

@available(iOS 15.0, OSX 11.0, *)
struct FoolIsSearchingTestView: View {
    @State private var searchText = ""
    @State private var isSearching = false
    
    var body: some View {
        VStack {
            HStack {
                Text(isSearching ? "Searching..." : "Not searching")
            }
        }
        .foolIsSearching(isSearching: $isSearching)
        .searchable(text: $searchText)
    }
}

@available(iOS 15.0, OSX 11.0, *)
struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        FoolIsSearchingTestView()
    }
}
