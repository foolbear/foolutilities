//
//  File.swift
//  
//
//  Created by foolbear on 2023/3/21.
//

import Combine

extension Task {
    public func store(in cancellables: inout Set<AnyCancellable>) {
        asCancellable().store(in: &cancellables)
    }

    public func asCancellable() -> AnyCancellable {
        .init { self.cancel() }
    }
}
