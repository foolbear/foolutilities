//
//  File.swift
//  
//
//  Created by foolbear on 2023/3/21.
//

import Foundation
import SwiftUI

public extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}

public extension Int {
    var boolValue: Bool {
        return self != 0
    }
}
