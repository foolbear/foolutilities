//
//  FoolCoreData.swift
//  FoolUtilities
//
//  Created by foolbear on 2020/12/10.
//

import SwiftUI
import CoreData

public extension NSManagedObjectContext {
    func saveChanges() {
        guard self.hasChanges else { return }
        do {
            try self.save()
        } catch {
            let error = error as NSError
            foolPrint("Unresolved error: \(error), \(error.userInfo)")
        }
    }
}
