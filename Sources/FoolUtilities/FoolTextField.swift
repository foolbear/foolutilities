//
//  FoolTextField.swift
//  FoolUtilities
//
//  Created by foolbear on 2020/1/9.
//

#if !os(macOS)

import SwiftUI

@available(iOS 16.0, *)
public struct FoolTextField: View {
    let hint: String
    let axis: Axis
    @Binding var text: String
    
    public init(_ hint: String, text: Binding<String>, axis: Axis = .horizontal) {
        self.hint = hint
        self._text = text
        self.axis = axis
    }
    
    public var body: some View {
        HStack {
            TextField(hint, text: $text, axis: axis)
            if text.isEmpty == false {
                Button {
                    text = ""
                } label: {
                    Image(systemName: "multiply.circle.fill")
                        .foregroundColor(.secondary)
                }
            }
        }.textFieldStyle(.roundedBorder)
    }
}


#endif
